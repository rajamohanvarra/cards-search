import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'countryFilter'
})
export class CountryFilterPipe implements PipeTransform {

  transform(allItems: any, countryName: string): any {
    if ( !allItems || !countryName) {
      return allItems;
    }

    // find
    // filter

    return allItems.filter( country =>
      country.name.toLowerCase().indexOf(countryName.toLowerCase()));
  }

}
