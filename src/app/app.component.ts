import {Component, OnInit} from '@angular/core';
import { HttpClient } from '@angular/common/http';


import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  selector: string = '.main-panel';
  title = 'countCards';
  dropdownSettings = {};
  today: number = Date.now();
  dropdownList = [];
  allItems: any;
  filteredItems: any;
  selectedItems = [];
  countryName: any;
  itemName: string;
  allItemsUrl = 'https://restcountries.eu/rest/v2/all';
  posts: any;

  constructor(private http: HttpClient) {}


  ngOnInit(): void {
    this.getAllItems();

    this.selectedItems = [];

    this.dropdownSettings = {
      singleSelection: false,
      text: 'Select-Countries',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: 'myclass custom-class'
    };
  }

  onItemSelect() {

    this.renderFun();

   // console.log(this.allItems.includes(this.itemName));

    // console.log(this.allItems[item].name);

    
    // this.selectedItems.forEach( element => {
    //   if(this.selectedItems['itemName'] == this.allItems.name){
    //     console.log(this.allItems.itemName);
    //   }  
    //   });

    // this.http.get(this.allItemsUrl + item.value).subscribe(
    //   data => {
    //     this.selectedItems .map((data) => {

    //     });

    //   }
    // );
    // this.allItems = this.selectedItems;
    // console.log(this.allItems);
    // this.allItems = this.selectedItems;
    // getItemsByName(this.selectedItems);
    // this.allItems = this.selectedItems;
    // console.log(this.allItems);

  }
  OnItemDeSelect(item: any) {
    console.log(item);
   console.log(this.selectedItems);

   if( this.selectedItems.length > 0) {
     this.renderFun();
  }  else {
    this.getAllItems();
  }
  }
  onSelectAll(items: any) {
    console.log(items);
    // this.getAllItems();

console.log('exicuted select');
  }
  onDeSelectAll(items: any) {
    console.log(items);
    this.getAllItems();
    // console.log('exicuted deselect');
  }

// for (var _i = 0, _a = map.keys(); _i < _a.length; _i++) {
//   var key = _a[_i];
//   console.log(key);
// }

renderFun() {
  this.posts = [];
    this.selectedItems.forEach(selectedItem => {
      //console.log(this.allItems.filter(allItem => selectedItem.itemName === allItem.name));
    this.posts = [...this.posts, ...this.allItems.filter(allItem => selectedItem.itemName === allItem.name)]
    });
}

  getAllItems(): void {
    this.getAllItemsService().subscribe(
      (allItems) => {
        this.allItems = allItems;
        console.log(allItems);
        this.filteredItems = allItems;
        console.log(allItems);
        // slice
        this.posts = this.allItems.slice(0, 3);
        console.log(this.posts);
        this.allItems.map((key, value) => {
          // console.log(key.name , value);
          // creating object and pushing it //
          this.dropdownList.push({
            id: value,
            itemName: key.name
          });
        });

        // console.log(this.dropdownList);
      }
    );
  }



  getAllItemsService() {
    return this.http.get(this.allItemsUrl);
  }

  // getItemsByName() {
  //   return this.http.get(this.allItems + name);
  // }


  // searchItem() {
  //   console.log(this.allItems.name.toLowerCase());
  //   this.allItems = this.allItems.filter.;
  // }

  onScrollDown() {
    console.log('scrolled down!!');
    if(this.posts.length < this.allItems.length){  
      let len = this.posts.length;
 
      for(let i = len; i <= len+3; i++){
        this.posts.push(this.allItems[i]);
      }
      
    }
  }
 
  onScrollUp() {
    console.log('scrolled up!!');
  }

}
